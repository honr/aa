namespace basic {

Runtime::Runtime(int argc, char* argv[], char** envp) {
  if (argc <= 0) {
    return;  // Probably shouldn't happen.
  }
  size_t num_args = static_cast<size_t>(argc - 1);
  program_ = argv[0];
  args_.reserve(num_args);
  // Currently only allows flags of the form -x=y (i.e., always with =).
  for (size_t i = 0; i < num_args; ++i) {
    std::string arg = argv[i + 1];
    if (!arg.empty() && arg[0] == '-') {
      const size_t pos = (arg.size() > 1 && arg[1] == '-') ? 2 : 1;
      if (size_t xpos = arg.find("=", pos); xpos != std::string::npos) {
        const std::string& key = arg.substr(pos, xpos - pos);
        const std::string& value = arg.substr(xpos + 1);
        flags_[key] = value;
      }
    } else {
      args_.emplace_back(arg);
    }
  }
  for (char** envp_cur = envp; *envp_cur != nullptr; envp_cur++) {
    const std::string kv = std::string(*envp_cur);
    size_t i = kv.find("=");
    if (i == std::string::npos) {
      continue;
    }
    const std::string& key = kv.substr(0, i);
    const std::string& value = kv.substr(i + 1);
    envs_[key] = value;
  }
  home_ = std::string(getenv("HOME"));
  // home_ = std::string(getpwuid(getuid())->pw_dir);
}

std::string Runtime::ExpandPath(std::string_view path) {
  if (path == "~" || path.starts_with("~/")) {
    return home_ + std::string(path.substr(1));
  }
  return std::string(path);
}

// TODO: use dup2(stdout_fd, 1) and dup2(stderr_fd, 2).
// Arguments are passed by value because we need the copies.
std::string Runtime::ForkExecWait(const std::string program,
                                  const std::vector<std::string> args) {
  static int tracking = 0;
  tracking++;
  int childpid = fork();
  if (childpid == 0) {  // at the child
    size_t n = args.size();
    std::vector<char*> argv(n + 2);
    argv[0] = const_cast<char*>(program.c_str());
    for (size_t i = 0; i < n; i++) {
      argv[i + 1] = const_cast<char*>(args[i].c_str());
    }
    argv[n + 1] = nullptr;
    execv(argv[0], &argv[0]);
    return "";  // Dummy; execv() should never return.
  }
  int status = 0;
  waitpid(childpid, &status, 0);
  if (status == 0) {
    return "";
  }
  return "program " + program + " returned with status " +
         std::to_string(status);
}

// static
std::string Runtime::ReadFileToString(const std::string& filepath) {
  std::ifstream stream(filepath);
  std::string contents((std::istreambuf_iterator<char>(stream)),
                       std::istreambuf_iterator<char>());
  return contents;
}

}  // namespace basic
