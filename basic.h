// #include <pwd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <deque>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <numeric>
#include <optional>
#include <queue>
#include <set>
#include <streambuf>
#include <string>
#include <utility>
#include <vector>

namespace basic {

class Runtime {
 public:
  Runtime(int argc, char* argv[], char** envp);
  ~Runtime() {}
  std::string ExpandPath(std::string_view path);
  std::string ForkExecWait(const std::string program,
                           const std::vector<std::string> args);
  static std::string ReadFileToString(const std::string& filepath);
  std::string program() { return program_; }
  std::vector<std::string> args() { return args_; }
  std::map<std::string, std::string> envs() { return envs_; }
  std::map<std::string, std::string> flags() { return flags_; }
  std::string home() { return home_; }

 private:
  std::string program_;
  std::vector<std::string> args_;
  std::map<std::string, std::string> envs_;
  std::map<std::string, std::string> flags_;
  std::string home_;
};

}  // namespace basic
