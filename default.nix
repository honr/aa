{nixpkgs ? import <nixpkgs>{}}:

let
  pkgs = nixpkgs;
  # pkgs = nixpkgs.pkgsMusl;
  # pkgs = nixpkgs.pkgsStatic; # Based on musl

  llvmPkgs = nixpkgs.llvmPackages_latest;
  # llvmPkgs = nixpkgs.llvmPackages_14;
  customStdenv = llvmPkgs.libcxxStdenv;
  # customStdenv_14 = nixpkgs.llvmPackages_latests.libcxxStdenv;
  # customStdenv = nixpkgs.pkgsStatic.stdenv;
  # customStdenv = nixpkgs.clangStdenv;
  mkDerivation = customStdenv.mkDerivation;
in mkDerivation {
  name = "env";
  buildInputs = map (x: (x.override {stdenv = customStdenv;})) [
    llvmPkgs.libcxx
    llvmPkgs.libcxxabi
    # llvmPkgs.clang
    # llvmPkgs.bintools

    pkgs.abseil-cpp
    # pkgs.libtensorflow-bin
    pkgs.readline
    pkgs.editline
    pkgs.protobuf
    pkgs.zlib
    # pkgs.glibc.bin
  # ] ++ map (x: (x.override {stdenv = customStdenv;})) [
  #   pkgs.abseil-cpp
  ] ++ [
    llvmPkgs.lld
    pkgs.stdman
    pkgs.zsh
    pkgs.which
    # pkgs.gcc
    # pkgs.glibc.static
    # pkgs.glibc
    # pkgs.glibc.static
    # pkgs.uclibc
    # pkgs.musl
    # pkgs.gdb

    # pkgs.binutils
    # pkgs.gcc_latest
  ];
}

# Additionally, read these:
# https://blog.galowicz.de/2018/02/27/managing_libraries_with_nix/
# http://mjhoy.com/journal/2016/02/nix.html
# https://nixos.org/nix/manual/
