namespace bb {

eden::Err Resolver::processRule(const eden::Symbol target,
                                const eden::Node& node) {
  if (!node.IsList()) {
    return eden::Err(1, "node should be of type list");
  }
  auto it = node.Vector().begin();
  auto itEnd = node.Vector().end();
  eden::Symbol rule_class = (*it)->Get<eden::Symbol>();
  // We need a vector of dependencies here.
  if (++it == itEnd || !(*it)->IsVector()) {
    return eden::Err(1, "Missing array of dependencies");
  }
  std::unordered_set<eden::Symbol> deps;
  for (const eden::Node* node : (*it)->Vector()) {
    if (node->IsSymbol()) {
      deps.emplace(node->Get<eden::Symbol>());
    } else {
      return eden::Err(1, "Dependencies should be symbols");
    }
  }
  // Often there is a map of rule attributes here.
  std::map<std::string_view, eden::Node> attrs(module_attrs_.begin(),
                                               module_attrs_.end());
  if (++it != itEnd && (*it)->IsMap()) {
    for (const auto& [k, v] : (*it)->Map()) {
      if (!k->IsKeyword()) {
        return eden::Err(1, "Map key is expected to be a keyword here");
      }
      std::string_view key(k->Get<eden::Symbol>().name());
      attrs[key] = *v;
    }
  }

  std::unique_ptr<Rule> rule = CreateRule(rule_class, target, attrs);
  if (rule == nullptr) {
    return eden::Err(1, "Unknown rule class " + std::string(rule_class.name()));
  }
  if (auto err = rule->Init(); !err.ok()) {
    std::cerr << "Error: " << rule_class << " " << target
              << " failed to Init(): " << err << ".\n";
    return err;
  }
  rules_.emplace(target, std::move(rule));
  deps_.emplace(target, deps);
  return eden::Err::OK;
}

eden::Err Resolver::Init(const eden::Node& global_attrs_root) {
  global_attrs_.clear();
  if (!global_attrs_root.IsVector()) {
    return eden::Err(1, "global_attrs_root should be a vector");
  }
  if (auto v = global_attrs_root.Vector(); v.empty()) {
    return eden::Err(1, "global_attrs_root was empty");
  }
  for (const auto& [k, v] : global_attrs_root.Vector().at(0)->Map()) {
    if (!k->IsKeyword()) {
      return eden::Err(1, "Map keys should be keywords here");
    }
    global_attrs_.emplace(k->Get<eden::Symbol>().name(), *v);
  }
  const std::string bb_file = global_attrs_[":bb"].AsString();
  const std::string spec_str = basic::Runtime::ReadFileToString(bb_file);
  std::unique_ptr<eden::Node> spec_root = eden::Read(spec_str);
  // populate list of rules and parameters from the node tree.
  // Node tree should look like the following:
  // (module MODULE-NAME {:ATTR-KEY ATTR-VALUE ...}
  //   (RULENAME TARGET [DEP1 DEP2 ...] {:PARAMETER VALUE}))
  auto it = spec_root->Vector().cbegin();
  auto itEnd = spec_root->Vector().cend();
  if (it == itEnd) {
    return eden::Err(1, "Empty spec");  // Empty spec.
  }
  if ((*it)->IsMap()) {
    module_attrs_ = global_attrs_;  // Copy.
    for (const auto& [k, v] : (*it)->Map()) {
      if (!k->IsKeyword()) {
        return eden::Err(1, "Map key is expected to be a keyword here");
      }
      module_attrs_[k->AsString()] = *v;
    }
    ++it;
  }
  // // Apply flags (they merely override attributes).
  // for (const auto& [flag, value] : runtime->flags()) {
  //   const eden::Node& value_node = *eden::Read(value)->Vector()[0];
  //   module_attrs_.insert_or_assign(":" + flag, value_node);
  // }
  for (; it != itEnd; ++it) {
    if (!(*it)->IsSymbol()) {
      return eden::Err(1, "Target name is required here");
    }
    eden::Symbol target = (*it)->Get<eden::Symbol>();
    ++it;
    if (auto err = processRule(target, **it); !err.ok()) {
      return err;
    }
  }
  return eden::Err::OK;
}

namespace {
// Given the digraph F in which x->y means y should be resolved before x,
// create the reverse digraph B going from y to x.  Given B find out the
// leaves (nodes with no outgoing edges), and put them in the layer (phase) 1.
// Then remove the leaves from the dependencies of the cover of leaves, and
// repeat the process.
std::pair<eden::Err, std::vector<std::unordered_set<eden::Symbol>>>
sortIntoPhases(
    const std::unordered_set<eden::Symbol>& targets,
    std::unordered_map<eden::Symbol, std::unordered_set<eden::Symbol>> F) {
  std::vector<std::unordered_set<eden::Symbol>> phases;
  std::unordered_set<eden::Symbol> seen;
  std::unordered_set<eden::Symbol> leaves;
  std::unordered_map<eden::Symbol, std::unordered_set<eden::Symbol>> rdepends;
  std::deque<eden::Symbol> q_init(targets.cbegin(), targets.cend());
  for (std::queue<eden::Symbol> q(q_init); !q.empty();) {
    const eden::Symbol target = q.front();
    q.pop();
    if (!seen.insert(target).second) {
      continue;
    }
    const auto it_deps = F.find(target);
    if (it_deps == F.end()) {
      return make_pair(
          eden::Err(1, "Couldn't find target " + std::string(target.name()) +
                           " in rules."),
          phases);
    }
    const std::unordered_set<eden::Symbol>& deps = it_deps->second;
    if (deps.empty()) {
      leaves.insert(target);
      continue;
    }
    for (const eden::Symbol dep : deps) {
      if (F.find(dep) == F.end()) {
        return make_pair(
            eden::Err(1, "Couldn't find dep " + std::string(dep.name()) +
                             " in rules."),
            phases);
      }
      q.push(dep);
      rdepends[dep].insert(target);
    }
  }

  while (!leaves.empty()) {
    phases.push_back(leaves);
    std::unordered_set<eden::Symbol> leaves_cover;
    for (const eden::Symbol leaf : leaves) {
      const std::unordered_set<eden::Symbol>& leaf_cover = rdepends[leaf];
      leaves_cover.insert(leaf_cover.cbegin(), leaf_cover.cend());
    }
    std::unordered_set<eden::Symbol> next_leaves;
    for (const eden::Symbol x : leaves_cover) {
      auto it_deps = F.find(x);
      if (it_deps == F.end()) {
        return make_pair(
            eden::Err(1, "Couldn't find " + std::string(x.name()) + " in F."),
            phases);
      }
      std::unordered_set<eden::Symbol>* deps = &(it_deps->second);
      for (const eden::Symbol leaf : leaves) {
        deps->erase(leaf);
      }
      if (deps->empty()) {
        next_leaves.insert(x);
      }
    }
    leaves = next_leaves;
  }

  return make_pair(eden::Err::OK, phases);
}
}  // namespace

eden::Err Resolver::Derive(const std::unordered_set<eden::Symbol>& target_set,
                           basic::Runtime* runtime) {
  auto err_and_phases = sortIntoPhases(target_set, deps_);
  if (auto err = err_and_phases.first; !err.ok()) {
    return err;
  }
  const std::vector<std::unordered_set<eden::Symbol>> phases =
      err_and_phases.second;

  std::unordered_map<Rule*, size_t> node_to_phase_map;
  for (size_t i = 0; i < phases.size(); ++i) {
    for (const eden::Symbol target : phases[i]) {
      // Assumes all targets in phases are in rules_.
      node_to_phase_map[rules_.at(target).get()] = i;
    }
  }

  std::vector<eden::Err> errs;
  for (size_t i = 0; i < phases.size(); ++i) {
    const std::unordered_set<eden::Symbol>& phase_targets = phases[i];
    std::cout << "Phase " << i + 1 << ":\n";
    for (const eden::Symbol target : phase_targets) {
      auto it = rules_.find(target);
      if (it == rules_.end()) {
        std::cerr << "Couldn't find target " << target << " in rules.\n";
        continue;
      }
      Rule* rule = it->second.get();
      if (rule == nullptr) {
        std::cerr << "Target " << target << " was nil in rules.\n";
        continue;
      }
      std::vector<Rule*> rule_deps;
      for (const eden::Symbol dep : deps_[target]) {
        rule_deps.push_back(rules_[dep].get());
      }
      std::sort(rule_deps.begin(), rule_deps.end(),
                [node_to_phase_map](Rule* a, Rule* b) {
                  return node_to_phase_map.at(a) < node_to_phase_map.at(b);
                });
      if (auto err = rule->Derive(rule_deps, runtime); !err.ok()) {
        std::cerr << "  [target=" << target << "] " << err << "\n";
        errs.push_back(err);
      }
    }
  }
  if (!errs.empty()) {
    return eden::Err(1, "Failed to derive targets");
  }
  return eden::Err::OK;
}

std::vector<eden::Symbol> Resolver::ListAvailableRules() const {
  std::vector<eden::Symbol> coll;
  for (const auto& [k, _] : rules_) {
    coll.push_back(k);
  }
  std::sort(coll.begin(), coll.end());
  return coll;
}

}  // namespace bb
