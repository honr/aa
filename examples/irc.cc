// A somewhat minimal IRC client.
// Example usage: irc NICKNAME@irc.freenode.net:6667

namespace irc {
const char kSpaceChars[] = " \t\n\r\v\f";
// Parting message used when none is specified with the "/part ..." command.
const char kDefaultPartingMessage[] = "Laters.";

struct Params {
  std::string Nick;
  std::string Host;
  std::string Port;
};

std::optional<Params> parseParams(const string& spec) {
  size_t atsign_pos = spec.find('@');
  size_t colon_pos = spec.find(':');
  if (atsign_pos == spec.npos || colon_pos == spec.npos ||
      atsign_pos >= colon_pos) {
    return {};
  }
  const std::string nick = spec.substr(0, atsign_pos);
  const std::string host =
      spec.substr(atsign_pos + 1, colon_pos - atsign_pos - 1);
  const std::string port = spec.substr(colon_pos + 1);
  Params params = {.Nick = nick, .Host = host, .Port = port};
  return params;
}

void Whine(const std::string& channel, const std::string& msg) {
  time_t t = time(nullptr);
  static char timestr[80];
  static char time_format[] = "%R";
  strftime(timestr, sizeof timestr, time_format, localtime(&t));

  fprintf(stdout, "\033[38;5;235m%s %-12s:\033[0m %s\n", timestr,
          channel.c_str(), msg.c_str());
}

class Client {
 public:
  Client() : nick_(""), host_(""), channel_("") {
    verbs_ = {
        "/join",  // local, masking /JOIN
        "/quit",  // local, masking /QUIT
        "/part",  // local, masking /PART
        "/msg",   // local, similart to /PRIVMSG
        "/exit",  // local, exit the program.
        //
        "/accept",     // remote
        "/admin",      // remote
        "/away",       // remote
        "/challenge",  // remote
        "/cmode",      // remote
        "/cnotice",    // remote
        "/cprivmsg",   // remote
        "/credits",    // remote
        "/error",      // remote
        "/extban",     // remote
        "/help",       // remote
        "/index",      // remote
        "/info",       // remote
        "/invite",     // remote
        "/ison",       // remote
        "/JOIN",       // remote
        "/kick",       // remote
        "/knock",      // remote
        "/links",      // remote
        "/list",       // remote
        "/lusers",     // remote
        "/map",        // remote
        "/motd",       // remote
        "/names",      // remote
        "/nick",       // remote
        "/notice",     // remote
        "/oper",       // remote
        "/PART",       // remote
        "/pass",       // remote
        "/ping",       // remote
        "/pong",       // remote
        "/privmsg",    // remote
        "/QUIT",       // remote
        "/stats",      // remote
        "/time",       // remote
        "/topic",      // remote
        "/trace",      // remote
        "/umode",      // remote
        "/user",       // remote
        "/userhost",   // remote
        "/users",      // remote
        "/version",    // remote
        "/who",        // remote
        "/whois",      // remote
        "/whowas",     // remote
    };
  }
  ~Client() {}

  void Dial(const Params& params) {
    nick_ = params.Nick;
    host_ = params.Host;

    int server_socket = 0;
    {
      struct addrinfo hints;
      struct addrinfo* res;
      struct addrinfo* r;
      memset(&hints, 0, sizeof hints);
      hints.ai_family = AF_UNSPEC;
      hints.ai_socktype = SOCK_STREAM;
      if (getaddrinfo(host_.c_str(), params.Port.c_str(), &hints, &res) != 0) {
        fprintf(stderr, "error: cannot resolve hostname '%s': %s\n",
                host_.c_str(), strerror(errno));
        exit(1);
      }
      for (r = res; r; r = r->ai_next) {
        if ((server_socket =
                 socket(r->ai_family, r->ai_socktype, r->ai_protocol)) == -1) {
          continue;
        }
        if (connect(server_socket, r->ai_addr, r->ai_addrlen) == 0) {
          break;
        }
        close(server_socket);
      }
      freeaddrinfo(res);
      if (!r) {
        fprintf(stderr, "error: cannot connect to host '%s' at port '%s': %s\n",
                host_.c_str(), params.Port.c_str(), strerror(errno));
        exit(1);
      }
    }

    server_fd_ = fdopen(server_socket, "r+");
    if (server_fd_ == 0) {
      fprintf(stderr, "error: fdopen: %s\n", strerror(errno));
      exit(1);
    }
    Send("NICK", nick_);
    Send("USER", nick_ + " localhost " + host_, nick_);
    fflush(server_fd_);
    setbuf(stdout, NULL);
    setbuf(server_fd_, NULL);
    setbuf(stdin, NULL);
  }

  enum class LocalResult {
    OK,
    EMPTY_LINE,
    END_OF_INPUT,
    EXIT_PROGRAM,
  };

  LocalResult HandleLocalInput(const char* line_p) {
    if (line_p == nullptr) {
      fprintf(stderr, "^D\n");
      return LocalResult::END_OF_INPUT;
    }
    const std::string line = line_p;

    const size_t kChannelMax = 256;
    if (line.empty()) {
      return LocalResult::EMPTY_LINE;
    }
    if (line[0] != '/') {
      PrivMsg(channel_, line);
      return LocalResult::OK;
    }

    if (line.starts_with("/join ")) {
      channel_ = line.substr(sizeof "/join " - 1);
      if (channel_.size() > kChannelMax) {
        channel_ = channel_.substr(0, kChannelMax - 1);
      }
      Send("JOIN", channel_);
      return LocalResult::OK;
    }

    // "/part" or "/part channel", or "/part channel parting message".
    if (line == "/part" || line.starts_with("/part ")) {
      std::string channel = channel_;
      std::string parting_message = kDefaultPartingMessage;
      size_t beg1 = line.find_first_not_of(kSpaceChars, sizeof "/part " - 1);
      if (beg1 != std::string::npos) {
        size_t end1 = line.find_first_of(kSpaceChars, beg1);
        if (end1 == std::string::npos) {
          channel = line.substr(beg1);
        } else {
          channel = line.substr(beg1, end1 - beg1);
          size_t beg2 = line.find_first_not_of(kSpaceChars, end1 + 1);
          if (beg2 != std::string::npos) {
            parting_message = line.substr(beg2);
          }
        }
      }
      Send("PART", channel, parting_message);
      return LocalResult::OK;
    }

    if (line.starts_with("/msg ")) {
      size_t beg1 = line.find_first_not_of(kSpaceChars, sizeof "/msg" - 1);
      if (beg1 == std::string::npos) {
        return LocalResult::OK;
      }
      size_t end1 = line.find_first_of(kSpaceChars, beg1);
      if (end1 == std::string::npos) {
        return LocalResult::OK;
      }
      std::string to = line.substr(beg1, end1 - beg1);
      size_t beg2 = line.find_first_not_of(kSpaceChars, end1 + 1);
      if (beg2 == std::string::npos) {
        return LocalResult::OK;
      }
      std::string message = line.substr(beg2);
      PrivMsg(to, message);
      return LocalResult::OK;
    }

    if (line.starts_with("/channel ")) {
      channel_ = line.substr(sizeof "/channel " - 1);
      if (channel_.size() > kChannelMax) {
        channel_ = channel_.substr(0, kChannelMax - 1);
      }
      return LocalResult::OK;
    }

    if (line == "/exit") {
      return LocalResult::EXIT_PROGRAM;
    }
    Send(line.substr(1).c_str());  // drop the '/' prefix.
    return LocalResult::OK;
  }

  std::vector<std::string> Complete(const string& prefix) const {
    std::vector<std::string> results;
    for (const std::string& verb : verbs_) {
      if (verb.starts_with(prefix)) {
        results.push_back(verb);
      }
    }
    return results;
  }

  void PrivMsg(const std::string& channel, const std::string& msg) {
    if (channel.empty()) {
      Whine("", "No channel to send to");
      return;
    }
    Whine(channel, "<" + nick_ + "> " + msg);
    Send("PRIVMSG", channel, msg);
  }

  void Send(const std::string& cmd) {
    std::fprintf(server_fd_, "%s\r\n", cmd.c_str());
  }
  void Send(const std::string& cmd, const std::string& arg1) {
    std::fprintf(server_fd_, "%s %s\r\n", cmd.c_str(), arg1.c_str());
  }
  void Send(const std::string& cmd, const std::string& arg1,
            const std::string& arg2) {
    std::fprintf(server_fd_, "%s %s :%s\r\n", cmd.c_str(), arg1.c_str(),
                 arg2.c_str());
  }

  enum class ServerResult {
    OK,
    CLOSED_CONNECTION,
    PARSE_ERROR,
  };

  struct ServerResponse {
    std::string User;
    std::string Command;
    std::string Arguments;
    std::string Text;
  };

  // format: "[:USER!JUNK ]CMD CMD_ARGUMENT[ ]:TXT\r"
  std::optional<ServerResponse> ParseServerResponse(std::string s) {
    ServerResponse response = {
        .User = "", .Command = "", .Arguments = "", .Text = ""};
    if (s.empty()) {
      return response;
    }
    size_t a = 0;  // beginning
    // Drop after \r or \n.
    if (size_t b = s.find_first_of("\r\n"); b != std::string::npos) {
      s = s.substr(0, b);
    }
    size_t e = s.size();

    if (s[0] == ':') {
      a++;
      size_t b;
      std::string sv;
      if (b = s.find_first_of(kSpaceChars, a); b == std::string::npos) {
        b = e;
      }
      sv = s.substr(a, b - a);
      a = b;
      if (size_t c = sv.find_first_of("!"); c != std::string::npos) {
        sv = sv.substr(0, c);
      }
      response.User = sv;
    } else {
      response.User = host_;
    }
    if (a == e) {
      return response;
    }
    // skip space
    if (a = s.find_first_not_of(kSpaceChars, a); a == std::string::npos) {
      return response;
    }
    // next token: Command
    {
      size_t b;
      if (b = s.find_first_of(kSpaceChars, a); b == std::string::npos) {
        b = e;
      }
      response.Command = s.substr(a, b - a);
      a = b;
    }
    if (a == e) {
      return response;
    }
    // skip space
    if (a = s.find_first_not_of(kSpaceChars, a); a == std::string::npos) {
      return response;
    }
    // next token: Arguments
    {
      size_t b;
      if (b = s.find_first_of(":", a); b == std::string::npos) {
        b = e;
      }
      std::string sv = s.substr(a, b - a);
      a = b;
      sv = sv.substr(0, 1 + sv.find_last_not_of(kSpaceChars));
      response.Arguments = sv;
    }
    if (a == e) {
      return response;
    }
    a++;
    response.Text = s.substr(a, e - a);
    return response;
  }

  // Returns false when the remote server closes the connection.
  ServerResult HandleServerResponse() {
    if (fgets(server_response_buf_, sizeof server_response_buf_, server_fd_) ==
        nullptr) {
      fprintf(stderr, "irc: remote host closed connection: %s\n",
              strerror(errno));
      return ServerResult::CLOSED_CONNECTION;
    }
    // null-terminate.
    server_response_buf_[sizeof server_response_buf_ - 1] = '\0';
    std::string response_buffer = server_response_buf_;
    auto response_or = ParseServerResponse(response_buffer);
    if (!response_or.has_value()) {
      return ServerResult::PARSE_ERROR;
    }
    const ServerResponse r = response_or.value();
    if (r.Command == "PONG") {
      return ServerResult::OK;
    }
    if (r.Command == "PRIVMSG") {
      Whine(r.Arguments, "<" + r.User + "> " + r.Text);
      return ServerResult::OK;
    }
    if (r.Command == "PING") {
      Send("PONG", r.Text);
      return ServerResult::OK;
    }
    if (r.Command == "JOIN") {
      Whine("",
            "\033[38;5;232m" + r.User + " joined " + r.Arguments + ".\033[0m");
      return ServerResult::OK;
    }
    if (r.Command == "QUIT") {
      Whine("", "\033[38;5;232m" + r.User + " quit.\033[0m");
      return ServerResult::OK;
    }
    if (r.Command == "PART") {
      Whine("", "\033[38;5;232m" + r.User + " departed from " + r.Arguments +
                    ".\033[0m");
      return ServerResult::OK;
    }

    Whine(r.User, "\033[38;5;233m• " + r.Command + " (" + r.Arguments +
                      ")\033[38;5;238m " + r.Text + "\033[0m");

    if (r.Command == "NICK" && r.User == nick_) {
      nick_ = r.Text;
    }
    return ServerResult::OK;
  }

  void InputLoop() {
    time_t t_last_server_response;
    struct timeval deadline;
    fd_set fds_to_read;
    for (;;) {
      FD_ZERO(&fds_to_read);
      FD_SET(0, &fds_to_read);
      FD_SET(fileno(server_fd_), &fds_to_read);
      deadline = {.tv_sec = 120, .tv_usec = 0};  // 2 minutes.
      int n = select(fileno(server_fd_) + 1, &fds_to_read, 0, 0, &deadline);
      if (n < 0) {
        if (errno == EINTR) {
          continue;
        }
        fprintf(stderr, "irc: error on select(): %s\n", strerror(errno));
        break;
      }
      if (n == 0) {
        if (time(nullptr) - t_last_server_response >= 300) {  // 5 minutes
          fprintf(stderr, "irc: shutting down, past timeout: %s\n",
                  strerror(errno));
          break;
        }
        Send("PING", host_);
        continue;
      }
      if (FD_ISSET(fileno(server_fd_), &fds_to_read)) {
        rl_save_prompt();
        if (HandleServerResponse() == irc::Client::ServerResult::OK) {
          t_last_server_response = time(nullptr);
        }
        rl_restore_prompt();
        rl_on_new_line();
        rl_redisplay();
      }
      if (FD_ISSET(0, &fds_to_read)) {
        rl_callback_read_char();
      }
    }
  }

 private:
  FILE* server_fd_;
  std::string nick_;
  std::string host_;
  std::string channel_;
  char server_response_buf_[4096];
  std::vector<std::string> verbs_;
};
}  // namespace irc

namespace {
irc::Client* GLOBAL_irc = nullptr;
char* GLOBAL_history_file = nullptr;

void usage(const char* arg0) {
  std::cerr << "Usage:\n"
            << arg0 << " NICK@SERVER:PORT, E.g.:\n"
            << arg0 << " NICK@irc.freenode.net:6667\n";
}
}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    usage(argv[0]);
    return 1;
  }
  std::string spec = argv[1];
  const auto params_or = irc::parseParams(spec);
  if (!params_or.has_value()) {
    usage(argv[0]);
  }
  const irc::Params& params = params_or.value();
  const std::string history_file = os::ExpandDir("~/.history/irc");
  GLOBAL_history_file = strdup(history_file.c_str());
  std::string prompt = "irc> ";  // "\x1b[38;5;%dmirc\x1b[0m> ", 1*36+2*6+3

  irc::Client irc;
  GLOBAL_irc = &irc;

  using_history();
  read_history(history_file.c_str());
  rl_attempted_completion_function = [](const char* prefix, int /*start*/,
                                        int /*end*/) -> char** {
    rl_attempted_completion_over = 1;  // Don't complete filenames.
    return rl_completion_matches(
        prefix, [](const char* prefix, int state) -> char* {
          static std::vector<std::string> matches;
          static size_t cur = 0;
          if (state == 0) {  // First time looking at this prefix.
            cur = 0;
            matches = GLOBAL_irc->Complete(std::string(prefix));
          }
          if (cur >= matches.size()) {
            return nullptr;  // No more matches.
          }
          return strdup(matches[cur++].c_str());  // Caller owns the copy.
        });
  };

  rl_callback_handler_install(prompt.c_str(), [](char* line) {
    if (GLOBAL_irc == nullptr) {
      std::cerr << "IRC client not initialized\n";
      exit(1);
    }
    switch (GLOBAL_irc->HandleLocalInput(line)) {
      case irc::Client::LocalResult::OK:
        add_history(line);
        write_history(GLOBAL_history_file);
        break;
      case irc::Client::LocalResult::END_OF_INPUT:
        break;
      case irc::Client::LocalResult::EMPTY_LINE:
        break;
      case irc::Client::LocalResult::EXIT_PROGRAM:
        exit(0);
    }
  });

  for (;;) {
    irc.Dial(params);
    irc.InputLoop();
  }
}
