TEST(Eden, ReadPrint) {
  const std::vector<std::pair<std::string, std::string>> cases = {
      {"(a (b))",
       "[(a\n"
       "  (b))]"},
      {"\"A\"; foo bar baz\n\"B\"",
       "[\"A\"\n"
       " \"B\"]"},
      {"foo.bar.baz", "[foo.bar.baz]"},
      {"\"Hello World\\\"\\n\\t\\txyz\"",
       "[\"Hello World\\\"\n"
       "\t\txyz\"]"},
      {"\\space \\a \\b \\newline",
       "[\\space\n"
       " \\a\n"
       " \\b\n"
       " \\newline]"},
      {"(aa.bb.cc (b) {:a 1 :b \"Some string\"}) ;; some comments\n (x y)",
       "[(aa.bb.cc\n"
       "  (b)\n"
       "  {:a 1,\n"
       "   :b \"Some string\"})\n"
       " (x\n"
       "  y)]"},
      {"{:a 1 :b 2 :c \\o142}",
       "[{:a 1,\n"
       "  :b 2,\n"
       "  :c \\b}]"},
      {"[nil true false]",
       "[[nil\n  "
       "true\n  "
       "false]]"},
      {"{:a 1.0 :b -2.0}",
       "[{:a 1.000000,\n"
       "  :b -2.000000}]"},
      {"1", "[1]"},
      {"\"\"", "[\"\"]"},
      {"\"\\\"\"", "[\"\\\"\"]"},
  };
  for (const auto& testcase : cases) {
    const std::string& in = testcase.first;
    const std::string& want = testcase.second;
    const std::string got = eden::PrettyPrint(*eden::Read(in));
    EXPECT_EQ(want, got);
  }
}

TEST(Eden, BbFromFile) {
  const std::string bb_contents = basic::Runtime::ReadFileToString("BB");
  const std::string prettyPrinted = eden::PrettyPrint(*eden::Read(bb_contents));
  std::cerr << prettyPrinted;
}

TEST(Eden, Word) {
  std::string_view sv = "String";
  eden::Word word = eden::Word(sv);
  EXPECT_EQ(sv, word.string_view());

  char c_str[] = "String";
  eden::Word word2 = eden::Word(c_str);
  EXPECT_EQ(sv, word2.string_view());

  EXPECT_TRUE(8 == sizeof(eden::Word));

  eden::Word w1 = "Foo";
  eden::Word w2 = "Bar";
  EXPECT_FALSE(w1 == w2);
}

TEST(Eden, Iterator) {
  auto v_list = eden::Read("[1 2 3]");
  auto v = v_list->Vector().at(0);
  for (const auto& x : v->Vector()) {
    std::cerr << "index: " << PrettyPrint(*x) << "\n";
  }
}
