namespace bb {

// TODO:
// Node:
//   - Derivation rule
//   - Qualified path (QP)
//   - Instance hash (IH)
// ----
// Node::Derive() should have the elaborate logic for dealing with
// various dependency types.  (lib <- src, hdr, lib, inc).
// Derive: Copy to tmpdir, derive, store, record IH.
//
// ForkExecWait should record stderr, stdout, returned code, and log to a
// log-journal.

class Rule {  // Interface
 public:
  virtual ~Rule() {}
  virtual eden::Err Init() = 0;
  virtual eden::Err Derive(const std::vector<Rule*>& deps,
                           basic::Runtime* runtime) = 0;
  // This could probably use std::variant.
  enum class Type {
    FILE = 0,
    CPP_LIB = 1,
    CPP_BIN = 2,
    CPP_RUN = 3,
    CPP_PILE = 4,
  };
  virtual Type type() const = 0;
};

template <Rule::Type T>
class TypedRule : public Rule {
  Rule::Type type() const final { return T; }
};

class File : public TypedRule<Rule::Type::FILE> {
 public:
  eden::Err Init() override { return eden::Err::OK; }
  eden::Err Derive(const std::vector<Rule*>& deps __attribute__((unused)),
                   basic::Runtime* runtime __attribute__((unused))) override {
    return eden::Err::OK;
  }
};

class CppLib : public TypedRule<Rule::Type::CPP_LIB> {
 public:
  CppLib(const eden::Symbol name,
         const std::map<std::string_view, eden::Node>& attrs);
  eden::Err Init() override;
  eden::Err Derive(const std::vector<Rule*>& deps,
                   basic::Runtime* runtime) override;
  std::filesystem::path ObjFile(const std::filesystem::path& obj_dir) const;
  const std::vector<std::filesystem::path>& header_files() const {
    return header_files_;
  }

 private:
  const eden::Symbol name_;
  const std::map<std::string_view, eden::Node> attrs_;
  std::vector<std::filesystem::path> srcs_;
  std::filesystem::path obj_file_;
  std::vector<std::filesystem::path> header_files_;
};

class CppPile : public TypedRule<Rule::Type::CPP_PILE> {
 public:
  CppPile(const eden::Symbol name,
          const std::map<std::string_view, eden::Node>& attrs);
  eden::Err Init() override { return eden::Err::OK; }
  eden::Err Derive(const std::vector<Rule*>& deps __attribute__((unused)),
                   basic::Runtime* runtime __attribute__((unused))) override {
    return eden::Err::OK;
  }
  const std::set<std::vector<std::string>>& cflags_set() const {
    return cflags_set_;
  }
  const std::set<std::string>& lib_set() const { return lib_set_; }

 private:
  const eden::Symbol name_;
  std::set<std::vector<std::string>> cflags_set_;
  std::set<std::string> lib_set_;
};

class CppBin : public TypedRule<Rule::Type::CPP_BIN> {
 public:
  CppBin(const eden::Symbol name,
         const std::map<std::string_view, eden::Node>& attrs)
      : name_(name), attrs_(attrs) {}
  eden::Err Init() override;
  eden::Err Derive(const std::vector<Rule*>& deps,
                   basic::Runtime* runtime) override;

 private:
  const eden::Symbol name_;
  const std::map<std::string_view, eden::Node> attrs_;
  std::vector<std::filesystem::path> srcs_;
};

class CppRun : public TypedRule<Rule::Type::CPP_RUN> {
 public:
  CppRun(const eden::Symbol name,
         const std::map<std::string_view, eden::Node>& attrs)
      : name_(name), attrs_(attrs) {}
  eden::Err Init() override;
  eden::Err Derive(const std::vector<Rule*>& deps,
                   basic::Runtime* runtime) override;

 private:
  const eden::Symbol name_;
  const std::map<std::string_view, eden::Node> attrs_;
  std::vector<std::filesystem::path> srcs_;
};

std::unique_ptr<bb::Rule> CreateRule(
    const eden::Symbol rule_class, const eden::Symbol name,
    const std::map<std::string_view, eden::Node>& attrs);

eden::Err compileCpp(const std::vector<std::filesystem::path>& srcs,
                     const std::vector<std::filesystem::path>& includes,
                     const std::filesystem::path& o_file,
                     const std::map<std::string_view, eden::Node>& attrs,
                     basic::Runtime* runtime);

eden::Err linkCppBinary(const std::vector<std::filesystem::path>& oFiles,
                        const std::filesystem::path& binFile,
                        const std::map<std::string_view, eden::Node>& attrs,
                        basic::Runtime* runtime);

}  // namespace bb
