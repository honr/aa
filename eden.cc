#include <algorithm>
#include <cstring>
#include <iostream>

namespace eden {

void Init() {
  Symbol::Init();
  Err::Init();
}

std::vector<std::string> Err::msgs_;
std::unordered_map<std::string, size_t> Err::msgs_map_;
Err Err::OK(0, "");

void Err::Init() {
  Err::msgs_ = {""};
  Err::msgs_map_ = {{"", 0}};
  // Err::OK = Err{.code_ = 0, .index_ = 0};
}

Err::Err(int code, std::string_view msg) : code_(code) {
  if (auto it = msgs_map_.find(std::string(msg)); it != msgs_map_.end()) {
    index_ = it->second;
    return;
  }
  index_ = msgs_.size();
  msgs_.push_back(std::string(msg));  // TODO: mutex guard.
  msgs_map_[msgs_.back()] = index_;
}

std::vector<std::string> Symbol::names_;
std::unordered_map<std::string, size_t> Symbol::names_map_;

// static
void Symbol::Init() {
  Symbol::names_ = {"INVALID"};
  Symbol::names_map_ = {{Symbol::names_[0], 0}};
}

Symbol::Symbol(std::string_view name) {
  if (auto it = names_map_.find(std::string(name)); it != names_map_.end()) {
    index_ = it->second;
    return;
  }
  index_ = names_.size();
  names_.push_back(std::string(name));  // TODO: mutex guard.
  names_map_[names_.back()] = index_;
}

Word::Word(const char* c_str) {
  const size_t len = std::strlen(c_str);
  if (len <= 0 || len > 7) {
    return;
  }
  semantic_.len = static_cast<int8_t>(len);
  std::memcpy(semantic_.data, c_str, len);
}

Word::Word(const std::string_view sv) {
  const size_t len = sv.size();
  if (len <= 0 || len > 7) {
    return;
  }
  semantic_.len = static_cast<int8_t>(len);
  sv.copy(semantic_.data, len);
}

namespace {
class Reader {
 public:
  static std::unique_ptr<Node> Read(std::string_view s);

 private:
  enum Context {
    NORMAL = 0,
    STRING = 1,
    ESCAPING_STRING = 2,
    COMMENT = 3,
    TOKEN = 4,
  };

  Reader();

  static int Signals(const char c);
  bool RecordToken();
  void EatParenthesis(const char c);
  void StartMetadataMap();
  void StartEscapableQuote();
  void StartQuote();
  void StartUnquote();
  bool Eat(const char c);
  std::unique_ptr<Node> Release();

  Context context_;
  std::unique_ptr<Node> root_;
  std::vector<Node*> coll_stack_;
  Node::Type token_type_;  // Currently only nil, string, or symbol.
  std::string token_;
  std::string error_;
};

// TODO: return std::vector<std::unique_ptr<Node>>.
// static
std::unique_ptr<Node> Reader::Read(std::string_view s) {
  Reader reader;
  reader.Eat('[');
  for (const char c : s) {
    if (!reader.Eat(c)) {
      std::cerr << "Error: " << reader.error_ << "\n";
      return nullptr;
    }
  }
  if (!reader.Eat(']')) {
    std::cerr << "Error: " << reader.error_ << "\n";
    return nullptr;
  }
  return reader.Release();
}

Reader::Reader()
    : context_(Context::NORMAL),
      root_(nullptr),
      token_type_(Node::Type::Nil),
      token_(""),
      error_("") {}

// static
int Reader::Signals(const char c) {
  // [ 0 0 0 0 0 0 0 0 ]
  //           ^ ^ ^ ^
  //           | | | |
  //           | | | `-- non-control (alphanum, etc.) whitespace
  //           | | `---- paren, ", '.
  //           | `------ whitespace
  //            `------- control
  static constexpr int kSignals[256] = {
      // (dotimes (i 256) (insert (format "      0b00000000, // %3d (%c)\n" i
      // i)))
      0b00000000,  //   0
      0b00000000,  //   1
      0b00000000,  //   2
      0b00000000,  //   3
      0b00000000,  //   4
      0b00000000,  //   5
      0b00000000,  //   6
      0b00000000,  //   7
      0b00000000,  //   8
      0b00000100,  //   9 (\t)
      0b00000100,  //  10 (\n)
      0b00000000,  //  11
      0b00000000,  //  12
      0b00000100,  //  13 (\r)
      0b00000000,  //  14
      0b00000000,  //  15
      0b00000000,  //  16
      0b00000000,  //  17
      0b00000000,  //  18
      0b00000000,  //  19
      0b00000000,  //  20
      0b00000000,  //  21
      0b00000000,  //  22
      0b00000000,  //  23
      0b00000000,  //  24
      0b00000000,  //  25
      0b00000000,  //  26
      0b00000000,  //  27
      0b00000000,  //  28
      0b00000000,  //  29
      0b00000000,  //  30
      0b00000000,  //  31
      0b00000100,  //  32 ( )
      0b00000001,  //  33 (!)
      0b00000010,  //  34 (")
      0b00001000,  //  35 (#)
      0b00000001,  //  36 ($)
      0b00000001,  //  37 (%)
      0b00001000,  //  38 (&)
      0b00001000,  //  39 (')
      0b00000010,  //  40 (()
      0b00000010,  //  41 ())
      0b00000001,  //  42 (*)
      0b00000001,  //  43 (+)
      0b00000100,  //  44 (,) - Considered whitespace
      0b00000001,  //  45 (-)
      0b00000001,  //  46 (.)
      0b00000001,  //  47 (/)
      0b00000001,  //  48 (0)
      0b00000001,  //  49 (1)
      0b00000001,  //  50 (2)
      0b00000001,  //  51 (3)
      0b00000001,  //  52 (4)
      0b00000001,  //  53 (5)
      0b00000001,  //  54 (6)
      0b00000001,  //  55 (7)
      0b00000001,  //  56 (8)
      0b00000001,  //  57 (9)
      0b00000001,  //  58 (:)
      0b00001000,  //  59 (;)
      0b00000001,  //  60 (<)
      0b00000001,  //  61 (=)
      0b00000001,  //  62 (>)
      0b00000001,  //  63 (?)
      0b00000001,  //  64 (@)
      0b00000001,  //  65 (A)
      0b00000001,  //  66 (B)
      0b00000001,  //  67 (C)
      0b00000001,  //  68 (D)
      0b00000001,  //  69 (E)
      0b00000001,  //  70 (F)
      0b00000001,  //  71 (G)
      0b00000001,  //  72 (H)
      0b00000001,  //  73 (I)
      0b00000001,  //  74 (J)
      0b00000001,  //  75 (K)
      0b00000001,  //  76 (L)
      0b00000001,  //  77 (M)
      0b00000001,  //  78 (N)
      0b00000001,  //  79 (O)
      0b00000001,  //  80 (P)
      0b00000001,  //  81 (Q)
      0b00000001,  //  82 (R)
      0b00000001,  //  83 (S)
      0b00000001,  //  84 (T)
      0b00000001,  //  85 (U)
      0b00000001,  //  86 (V)
      0b00000001,  //  87 (W)
      0b00000001,  //  88 (X)
      0b00000001,  //  89 (Y)
      0b00000001,  //  90 (Z)
      0b00000010,  //  91 ([)
      0b00000001,  //  92 (\)
      0b00000010,  //  93 (])
      0b00001000,  //  94 (^)
      0b00000001,  //  95 (_)
      0b00001000,  //  96 (`)
      0b00000001,  //  97 (a)
      0b00000001,  //  98 (b)
      0b00000001,  //  99 (c)
      0b00000001,  // 100 (d)
      0b00000001,  // 101 (e)
      0b00000001,  // 102 (f)
      0b00000001,  // 103 (g)
      0b00000001,  // 104 (h)
      0b00000001,  // 105 (i)
      0b00000001,  // 106 (j)
      0b00000001,  // 107 (k)
      0b00000001,  // 108 (l)
      0b00000001,  // 109 (m)
      0b00000001,  // 110 (n)
      0b00000001,  // 111 (o)
      0b00000001,  // 112 (p)
      0b00000001,  // 113 (q)
      0b00000001,  // 114 (r)
      0b00000001,  // 115 (s)
      0b00000001,  // 116 (t)
      0b00000001,  // 117 (u)
      0b00000001,  // 118 (v)
      0b00000001,  // 119 (w)
      0b00000001,  // 120 (x)
      0b00000001,  // 121 (y)
      0b00000001,  // 122 (z)
      0b00000010,  // 123 ({)
      0b00000001,  // 124 (|)
      0b00000010,  // 125 (})
      0b00001000,  // 126 (~)
      0b00000000,  // 127
      0b00000000,  // 128
      0b00000000,  // 129
      0b00000000,  // 130
      0b00000000,  // 131
      0b00000000,  // 132
      0b00000000,  // 133
      0b00000000,  // 134
      0b00000000,  // 135
      0b00000000,  // 136
      0b00000000,  // 137
      0b00000000,  // 138
      0b00000000,  // 139
      0b00000000,  // 140
      0b00000000,  // 141
      0b00000000,  // 142
      0b00000000,  // 143
      0b00000000,  // 144
      0b00000000,  // 145
      0b00000000,  // 146
      0b00000000,  // 147
      0b00000000,  // 148
      0b00000000,  // 149
      0b00000000,  // 150
      0b00000000,  // 151
      0b00000000,  // 152
      0b00000000,  // 153
      0b00000000,  // 154
      0b00000000,  // 155
      0b00000000,  // 156
      0b00000000,  // 157
      0b00000000,  // 158
      0b00000000,  // 159
      0b00000000,  // 160
      0b00000001,  // 161 (¡)
      0b00000001,  // 162 (¢)
      0b00000001,  // 163 (£)
      0b00000001,  // 164 (¤)
      0b00000001,  // 165 (¥)
      0b00000001,  // 166 (¦)
      0b00000001,  // 167 (§)
      0b00000001,  // 168 (¨)
      0b00000001,  // 169 (©)
      0b00000001,  // 170 (ª)
      0b00000010,  // 171 («)
      0b00000001,  // 172 (¬)
      0b00000000,  // 173
      0b00000001,  // 174 (®)
      0b00000001,  // 175 (¯)
      0b00000001,  // 176 (°)
      0b00000001,  // 177 (±)
      0b00000001,  // 178 (²)
      0b00000001,  // 179 (³)
      0b00000010,  // 180 (´)
      0b00000001,  // 181 (µ)
      0b00000001,  // 182 (¶)
      0b00000001,  // 183 (·)
      0b00000001,  // 184 (¸)
      0b00000001,  // 185 (¹)
      0b00000001,  // 186 (º)
      0b00000010,  // 187 (»)
      0b00000001,  // 188 (¼)
      0b00000001,  // 189 (½)
      0b00000001,  // 190 (¾)
      0b00000001,  // 191 (¿)
      0b00000001,  // 192 (À)
      0b00000001,  // 193 (Á)
      0b00000001,  // 194 (Â)
      0b00000001,  // 195 (Ã)
      0b00000001,  // 196 (Ä)
      0b00000001,  // 197 (Å)
      0b00000001,  // 198 (Æ)
      0b00000001,  // 199 (Ç)
      0b00000001,  // 200 (È)
      0b00000001,  // 201 (É)
      0b00000001,  // 202 (Ê)
      0b00000001,  // 203 (Ë)
      0b00000001,  // 204 (Ì)
      0b00000001,  // 205 (Í)
      0b00000001,  // 206 (Î)
      0b00000001,  // 207 (Ï)
      0b00000001,  // 208 (Ð)
      0b00000001,  // 209 (Ñ)
      0b00000001,  // 210 (Ò)
      0b00000001,  // 211 (Ó)
      0b00000001,  // 212 (Ô)
      0b00000001,  // 213 (Õ)
      0b00000001,  // 214 (Ö)
      0b00000001,  // 215 (×)
      0b00000001,  // 216 (Ø)
      0b00000001,  // 217 (Ù)
      0b00000001,  // 218 (Ú)
      0b00000001,  // 219 (Û)
      0b00000001,  // 220 (Ü)
      0b00000001,  // 221 (Ý)
      0b00000001,  // 222 (Þ)
      0b00000001,  // 223 (ß)
      0b00000001,  // 224 (à)
      0b00000001,  // 225 (á)
      0b00000001,  // 226 (â)
      0b00000001,  // 227 (ã)
      0b00000001,  // 228 (ä)
      0b00000001,  // 229 (å)
      0b00000001,  // 230 (æ)
      0b00000001,  // 231 (ç)
      0b00000001,  // 232 (è)
      0b00000001,  // 233 (é)
      0b00000001,  // 234 (ê)
      0b00000001,  // 235 (ë)
      0b00000001,  // 236 (ì)
      0b00000001,  // 237 (í)
      0b00000001,  // 238 (î)
      0b00000001,  // 239 (ï)
      0b00000001,  // 240 (ð)
      0b00000001,  // 241 (ñ)
      0b00000001,  // 242 (ò)
      0b00000001,  // 243 (ó)
      0b00000001,  // 244 (ô)
      0b00000001,  // 245 (õ)
      0b00000001,  // 246 (ö)
      0b00000001,  // 247 (÷)
      0b00000001,  // 248 (ø)
      0b00000001,  // 249 (ù)
      0b00000001,  // 250 (ú)
      0b00000001,  // 251 (û)
      0b00000001,  // 252 (ü)
      0b00000001,  // 253 (ý)
      0b00000001,  // 254 (þ)
      0b00000001,  // 255 (ÿ)
  };
  return kSignals[static_cast<uint8_t>(c)];
}

char CharFromName(const std::string& token) {
  if (token.size() < 2) {
    return '\0';
  }
  if (token.size() == 2) {
    return token[1];
  }
  if (token.size() == 5 && token[1] == 'o') {  // Octal: \oNNN
    const char a = static_cast<char>(token[2] - '0');
    const char b = static_cast<char>(token[3] - '0');
    const char c = static_cast<char>(token[4] - '0');
    if (0 > a || a > 3 || 0 > b || b > 7 || 0 > c || c > 7) {
      return '\0';
    }
    return static_cast<char>(a << 6 | b << 3 | c);
  }
  if (token.size() == 6 && token[1] == 'u') {
    const char a = static_cast<char>(token[2] - '0');
    const char b = static_cast<char>(token[3] - '0');
    const char c = static_cast<char>(token[4] - '0');
    const char d = static_cast<char>(token[5] - '0');
    if (0 > a || a > 7 || 0 > b || b > 7 || 0 > c || c > 7 || 0 > d || d > 7) {
      return '\0';
    }
    // Unicode, \uNNNN as in Java.
    // TODO: return a << 24 | b << 16 | c << 8 | d;
    return 'U';
  }
  if (token == "\\newline") {
    return '\n';
  } else if (token == "\\space") {
    return ' ';
  } else if (token == "\\return") {
    return '\r';
  } else if (token == "\\tab") {
    return '\t';
  } else if (token == "\\formfeed") {
    return '\f';
  } else if (token == "\\backspace") {
    return '\b';
  } else {
    return '\0';
  }
}

Node* CreateNodeFromToken(const Node::Type initial_type,
                          const std::string& token) {
  if ((initial_type != Node::Type::String) &&
      (initial_type != Node::Type::Symbol || token.size() < 1)) {
    // This should not happen; maybe exit(1) is more appropriate.
    return nullptr;
  }

  if (initial_type == Node::Type::String) {
    return new Node{
        .type = Node::Type::String,
        .variant = static_cast<void*>(new std::string(token)),
    };
  }

  const char first_char = token[0];
  if (first_char == '\\') {
    return new Node{.type = Node::Type::Char, .variant = CharFromName(token)};
  }
  if (first_char == ':') {
    return new Node{.type = Node::Type::Keyword, .variant = Symbol(token)};
  }
  // Starts with a digit or (- or +) followed by a digit.
  if (('0' <= first_char && first_char <= '9') ||
      (token.size() > 1 && (first_char == '+' || first_char == '-') &&
       '0' <= token[1] && token[1] <= '9')) {
    if (token.find('.') != std::string::npos) {
      return new Node{.type = Node::Type::Float, .variant = std::stod(token)};
    }
    int64_t n = std::stoll(token);
    return new Node{.type = Node::Type::Int, .variant = n};
  }

  if (token == "true") {
    return new Node{.type = Node::Type::Bool, .variant = true};
  }
  if (token == "false") {
    return new Node{.type = Node::Type::Bool, .variant = false};
  }
  if (token == "nil") {
    return new Node{
        .type = Node::Type::Nil,
        .variant = static_cast<void*>(nullptr),
    };
  }
  return new Node{.type = Node::Type::Symbol, .variant = Symbol(token)};
}

bool Reader::RecordToken() {
  Node* token_node = CreateNodeFromToken(token_type_, token_);
  if (token_node == nullptr) {
    return false;
  }
  coll_stack_.back()->MutVector()->push_back(token_node);
  token_type_ = Node::Type::Nil;
  token_ = "";
  return true;
}

void Reader::EatParenthesis(const char c) {
  if (c == '(' || c == '[' || c == '{') {  // open paren
    auto* node = new Node();
    switch (c) {
      case '(':
        node->type = Node::Type::List;
        break;
      case '[':
        node->type = Node::Type::Vector;
        break;
      case '{':
        node->type = Node::Type::Map;
        break;
    }
    node->variant = static_cast<void*>(new std::vector<Node*>);
    if (root_ == nullptr) {
      root_.reset(node);
    } else {
      coll_stack_.back()->MutVector()->push_back(node);
    }
    coll_stack_.push_back(node);
  } else if (c == ')' || c == ']' || c == '}') {  // close paren
    auto* coll = coll_stack_.back();
    const Node::Type expected_type =
        (c == ')') ? Node::Type::List
                   : (c == ']') ? Node::Type::Vector :
                                /* c == '}' */ Node::Type::Map;
    if (coll->type != expected_type) {
      // This should be an error, not a print out.
      std::cerr << "Mismatched parens.  Tried to close a " << coll->Typename()
                << " with a '" << c << "'.";
    }
    if (coll->type == Node::Type::Map) {
      // Modify the coll contents to turn it into a map.
      auto vec = coll->Vector();
      auto m = new std::map<Node*, Node*>;
      for (auto it = vec.cbegin(); it != vec.cend(); ++it) {
        Node* key = *it;
        if (++it == vec.cend()) {
          std::cerr << "Missing value for the last key\n";
          break;
        }
        Node* value = *it;
        m->insert_or_assign(key, value);
      }
      // delete static_cast<std::vector<Node*>>(coll->variant);
      coll->variant = static_cast<void*>(m);
    }
    coll_stack_.pop_back();
  } else {
    std::cerr << "Unknown paren '" << c << "'.";
  }
}

void Reader::StartMetadataMap() {}

void Reader::StartEscapableQuote() {}

void Reader::StartQuote() {}

void Reader::StartUnquote() {}

bool Reader::Eat(const char c) {
  int s = Signals(c);
  if (context_ == Context::STRING) {
    if (c == '\\') {
      context_ = Context::ESCAPING_STRING;
    } else if (c == '"') {
      context_ = Context::NORMAL;
      RecordToken();
    } else {
      token_ += c;
    }
    return true;
  }

  if (context_ == Context::ESCAPING_STRING) {
    context_ = Context::STRING;
    token_ +=
        (c == 'n' ? '\n'
                  : c == 't' ? '\t' : c == 'r' ? '\r' : c == 'f' ? '\f' : c);
    return true;
  }

  if (context_ == Context::COMMENT) {
    if (c == '\n') {
      context_ = Context::NORMAL;
    }
    return true;
  }

  if (s & 1) {
    if (context_ == Context::NORMAL) {
      context_ = Context::TOKEN;
      token_type_ = Node::Type::Symbol;
      token_ = "";
    }
    token_ += c;
    return true;
  }

  // s & 1 == 0, context is either NORMAL or TOKEN.

  if (s & 4) {                          // whitespace
    if (context_ == Context::NORMAL) {  // more whitespace
      return true;
    }
    if (context_ == Context::TOKEN) {  // control-ish
      RecordToken();
      context_ = Context::NORMAL;
      return true;
    }
    std::cerr << "Unexpected context for a whitespace\n";
  }

  if (context_ == Context::TOKEN) {  // control-ish
    RecordToken();  // Do not return, we need to react to `c' further down.
  }

  if (c == '"') {
    context_ = Context::STRING;
    token_type_ = Node::Type::String;
    token_ = "";
    return true;
  }

  if (c == ';') {
    context_ = Context::COMMENT;
    return true;
  }

  if (s & 2) {  // Paren start or end
    EatParenthesis(c);
    context_ = Context::NORMAL;
    return true;
  }

  if (c == '#') {
    //// context_ = Context::Normal;
    return true;
  }

  if (c == '^') {
    StartMetadataMap();
    context_ = Context::NORMAL;
    return true;
  }

  if (c == '`') {
    StartEscapableQuote();
    context_ = Context::NORMAL;
    return true;
  }

  if (c == '\'') {
    StartQuote();
    context_ = Context::NORMAL;
    return true;
  }

  if (c == '~') {
    StartUnquote();
    context_ = Context::NORMAL;
    return true;
  }

  std::cerr << "Unknown state: {:context " << context_ << ", :char '" << c
            << "'}\n";
  exit(1);
}

std::unique_ptr<Node> Reader::Release() { return std::move(root_); }
}  // namespace

std::unique_ptr<Node> Read(std::string_view s) { return Reader::Read(s); }

namespace {
std::string escapeQuotes(std::string_view before) {
  std::string after;
  for (std::string::size_type i = 0; i < before.length(); ++i) {
    const char c = before[i];
    if (c == '"' || c == '\\') {
      after += '\\';
    }
    after += c;
  }
  return after;
}

std::string charName(const char c) {
  switch (c) {
    case '\n':
      return "newline";
    case ' ':
      return "space";
    case '\t':
      return "tab";
    case '\f':
      return "formfeed";
    case '\b':
      return "backspace";
    case '\r':
      return "return";
    default:
      return std::string(1, c);
  }
}

}  // namespace

const std::string PrettyPrint(const Node& node, size_t indent) {
  std::string prefix(indent, ' ');
  if (node.IsCollection()) {
    std::string output;
    static const std::string paren_pairs[] = {"(", ")", "[",  "]",
                                              "{", "}", "#{", "}"};
    if (node.IsMap()) {
      output += "{";
      bool first_iteration = true;
      for (const auto& [k, v] : node.Map()) {
        if (first_iteration) {
          first_iteration = false;
        } else {
          output += ",\n" + prefix;
        }
        output +=
            PrettyPrint(*k, indent + 1) + " " + PrettyPrint(*v, indent + 1);
      }
      return output + "}";
    }
    const int paren_type =
        static_cast<int>(node.type) - static_cast<int>(Node::Type::List);
    output += paren_pairs[paren_type * 2];
    const std::vector<Node*>& values = node.Vector();
    bool first_iteration = true;
    for (auto it = values.cbegin(); it != values.cend(); ++it) {
      if (first_iteration) {
        first_iteration = false;
      } else {
        output += (node.IsMap() ? ",\n" : "\n") + prefix;
      }
      output += PrettyPrint(**it, indent + 1);
      if (node.IsMap()) {
        ++it;
        output += " " + PrettyPrint(**it, indent + 1);
      }
    }
    return output + paren_pairs[1 + paren_type * 2];
  }

  if (node.IsNil()) {
    return "nil";
  }

  if (node.IsString()) {
    return "\"" + escapeQuotes(node.AsString()) + "\"";
  }

  if (node.Holds<bool>()) {
    return node.Get<bool>() ? "true" : "false";
  }
  if (node.Holds<char>()) {
    return "\\" + charName(node.Get<char>());
  }
  if (node.Holds<int64_t>()) {
    return std::to_string(node.Get<int64_t>());
  }
  if (node.Holds<double>()) {
    return std::to_string(node.Get<double>());
  }
  if (node.Holds<Symbol>()) {
    return std::string(node.Get<Symbol>().name());
  }

  return "";
}

std::string Node::AsString() const {
  if (Holds<Symbol>()) {  // Includes Keyword
    return std::string(Get<Symbol>().name());
  }
  if (IsString()) {
    return *static_cast<std::string*>(std::get<void*>(variant));
  }
  return PrettyPrint(*this, /*indent=*/0);
}

std::ostream& operator<<(std::ostream& os, const Node& node) {
  os << PrettyPrint(node);
  return os;
}

}  // namespace eden
