//
// TODO:
//
// 1. Need some caching!  Read input from git, compile each piece, and then
//    store the blobs back in a parallel object store similar to git's.
//
// 2. Move individual resolvers to a separate file.  Either one file per rule,
//    or even a tiny set of functions in eden that expresses the resolvers.
//
// 3. Log output into a common place (in /var/log?  Or perhaps in
// ~/.local/var/bb?).
//
// 4. Keep an installation transcript that can be used for reverting steps of
//    installations.
//
// 5. Use a chroot or some other kind of isolation for the "run" resolver (not
//    implemented yet).
//
// 6. Need a way to avoid repeating parameters, for example, cflags of gmock.
//    Perhaps the flags should be part of depending on a c++lib.  Or maybe the
//    vendor (./v/...) libraries should only expose a single .h?  Or maybe
//    they should be "installed" in ./.include ?

// Perhaps 1 and 6 indicate we need a better DSL?

// "Compiler"/"linker" programs are essentially modelled `execve's.  The DSL
// should be able to express common invokations (std{in,out,err}, args,
// side-effect output files including temporary outputs (.o, .log, etc.).

struct Main {
  std::unique_ptr<basic::Runtime> runtime;
  static constexpr std::string_view kGlobalAttrsFile = "~/.config/bb/defaults";

  Main(int argc, char* argv[], char** envp) {
    eden::Init();
    runtime = std::make_unique<basic::Runtime>(argc, argv, envp);
  }

  int Run() {
    std::unordered_set<eden::Symbol> target_set;
    for (const std::string& target : runtime->args()) {
      target_set.insert(eden::Symbol(target));
    }
    std::unique_ptr<eden::Node> global_attrs_root =
        eden::Read(basic::Runtime::ReadFileToString(
            runtime->ExpandPath(kGlobalAttrsFile)));
    bb::Resolver resolver;
    if (auto err = resolver.Init(*global_attrs_root); !err.ok()) {
      std::cerr << err.msg() << "\n";
      return 1;
    }
    if (target_set.empty()) {
      for (const eden::Symbol target : resolver.ListAvailableRules()) {
        std::cout << "  " << target << "\n";
      }
      return 0;
    }
    if (auto err = resolver.Derive(target_set, runtime.get()); !err.ok()) {
      std::cerr << err.msg() << "\n";
      return 1;
    }
    return 0;
  }
};

int main(int argc, char* argv[], char** envp) {
  Main m(argc, argv, envp);
  return m.Run();
}
