namespace bb {
// Resolver has the list of rules (which nodes depend on which ones), and a
// Derive() that derives specified targets by scheduling in phases and calling
// each Rule's Derive() on its dependencies.
class Resolver {
 public:
  Resolver() {}
  ~Resolver() {}
  eden::Err Init(const eden::Node& global_attrs_root);
  std::vector<eden::Symbol> ListAvailableRules() const;
  eden::Err Derive(const std::unordered_set<eden::Symbol>& target_set,
                   basic::Runtime* runtime);

 private:
  eden::Err processRule(const eden::Symbol target, const eden::Node& rule);

  std::map<std::string, eden::Node> global_attrs_;
  std::map<std::string, eden::Node> module_attrs_;
  std::unordered_map<eden::Symbol, std::unique_ptr<Rule>> rules_;
  std::unordered_map<eden::Symbol, std::unordered_set<eden::Symbol>> deps_;
};
}  // namespace bb
