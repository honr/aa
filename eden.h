#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

namespace eden {

void Init();

// Represents a small (8 or 12 byte) interned string.
class Word {
 public:
  Word(const char* c_str);
  Word(const std::string_view sv);

  bool empty() { return semantic_.len == 0; }

  std::string_view string_view() const {
    return std::string_view(semantic_.data, static_cast<size_t>(semantic_.len));
  }
  friend struct std::hash<eden::Word>;
  bool operator==(const Word& other) const { return packed_ == other.packed_; }
  friend std::ostream& operator<<(std::ostream& os, const Word& w) {
    os << w.string_view();
    return os;
  }

 private:
  struct Semantic {
    int8_t len;
    char data[7];
  };
  union {
    int64_t packed_;
    Semantic semantic_;
  };
};

// An interned string.
// TODO: Expose the string interning pool as an optional template parameter,
// say, Symbol<PoolSingleton>.  Maybe also merge Word into Symbol and introduce
// a new num_bytes as another template parameter.
class Symbol {
 public:
  explicit Symbol(std::string_view name);
  std::string_view name() const { return names_.at(index_); }
  static void Init();
  friend struct std::hash<eden::Symbol>;
  bool operator==(const Symbol& other) const { return index_ == other.index_; }
  bool operator<(const Symbol other) const { return name() < other.name(); };
  friend std::ostream& operator<<(std::ostream& os, const Symbol& symbol) {
    os << symbol.name();
    return os;
  }
  operator uint64_t() { return index_; }  //

 private:
  size_t index_;
  static std::vector<std::string> names_;
  static std::unordered_map<std::string, size_t> names_map_;
};

class Err {
 public:
  Err(int code, std::string_view msg);
  ~Err() {}
  static void Init();
  std::string_view msg() const { return msgs_.at(index_); }
  bool ok() const { return code_ == 0; }

  bool operator==(const Err& other) const {
    return code_ == other.code_ && index_ == other.index_;
  }

  friend std::ostream& operator<<(std::ostream& os, const Err& err) {
    if (err.ok()) {
      os << "ok";
    } else {
      os << "error (" << err.msg() << ")";
    }
    return os;
  }

  static Err OK;

 private:
  int code_;
  size_t index_;
  static std::vector<std::string> msgs_;
  static std::unordered_map<std::string, size_t> msgs_map_;
};

// // Short Symbol, up to 7 ASCII characters.
// class Sym {
//  public:
//   Sym(const std::string_view sv) {
//     Word word(sv);
//     if (auto it = names_map_.find(word); it != names_map_.end()) {
//       index_ = it->second;
//       return;
//     }
//     index_ = names_.size();
//     names_.push_back(word);  // TODO: mutex guard.
//     names_map_[word] = index_;
//   }

//   std::string_view name() const {
//     return names_.at(index_).string_view();
//   }

//  private:
//   size_t index_;
//   static std::vector<eden::Word> names_;
//   static std::unordered_map<eden::Word, size_t> names_map_;
// };

// TODO: Maybe implement an iterator interface.
struct Node {
  enum class Type {
    // Atoms
    Nil = 0,
    Bool = 1,
    Char = 2,
    Int = 3,
    Float = 4,
    String = 5,
    // Symbols need a symbol table to "intern" into.
    Symbol = 6,
    Keyword = 7,

    // Collections
    List = 8,  // (type & Type::List) != 0 for collection types.
    Vector = 9,
    Map = 10,
    Set = 11,
  };
  Type type;

  // array must match the enum above.
  std::string_view Typename() const {
    static constexpr std::string_view typenames[] = {
        "nil",    "bool",    "char", "int",    "float", "string",
        "symbol", "keyword", "list", "vector", "map",   "set",
    };
    return typenames[static_cast<int>(type)];
  }

  std::variant<bool, char, int64_t, double, Word, Symbol, void*> variant;

  bool IsCollection() const {
    return static_cast<int>(type) & static_cast<int>(Node::Type::List);
  }

  const std::vector<Node*>& Vector() const {
    return *static_cast<std::vector<Node*>*>(Get<void*>());
  }

  std::vector<Node*>* MutVector() {
    return static_cast<std::vector<Node*>*>(Get<void*>());
  }

  const std::map<Node*, Node*>& Map() const {
    return *static_cast<std::map<Node*, Node*>*>(Get<void*>());
  }

  template <typename T>
  bool Holds() const {
    return std::holds_alternative<T>(variant);
  }

  template <typename T>
  T Get() const {
    return std::get<T>(variant);
  }

  bool IsNil() const { return type == Type::Nil; }
  bool IsString() const { return type == Type::String; }
  bool IsSymbol() const { return type == Type::Symbol; }
  bool IsKeyword() const { return type == Type::Keyword; }
  bool IsList() const { return type == Type::List; }
  bool IsVector() const { return type == Type::Vector; }
  bool IsMap() const { return type == Type::Map; }
  bool IsSet() const { return type == Type::Set; }

  std::string AsString() const;

  friend std::ostream& operator<<(std::ostream& os, const Node& node);
};

std::unique_ptr<Node> Read(std::string_view s);

const std::string PrettyPrint(const Node& node, size_t indent = 1);

}  // namespace eden

// Specialized hash functions
namespace std {
template <>
struct hash<eden::Symbol> {
  size_t operator()(const eden::Symbol& x) const {
    return std::hash<size_t>()(x.index_);
  }
};
template <>
struct hash<eden::Word> {
  size_t operator()(const eden::Word& word) const {
    return std::hash<int64_t>()(word.packed_);
  }
};

}  // namespace std
