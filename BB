{:acl []
 :out-dir "~/Gym/bb/.out/"
 :bin-dir "~/Gym/bb/.bin/"
 :install-dir "~/Gym/bb/.install/"
 :cflags-default ["-O3" ;; -Os, -Ofast.
                  "-fPIC"
                  "-fdata-sections"
                  "-ffunction-sections"
                  "-flto"
                  "-fno-exceptions"
                  "-fno-rtti"
                  "-fno-strict-aliasing"
                  "-fno-threadsafe-statics"
                  "-fstack-protector-all"
                  "-fvisibility-inlines-hidden"
                  "-fvisibility=hidden"
                  "-pedantic"
                  "-pipe"
                  "-std=c++2a"
                  "-Wall"
                  "-Wcast-align"
                  "-Wconversion"
                  "-Werror"
                  "-Wextra"
                  "-Wfloat-equal"
                  "-Wformat=2"
                  "-Winit-self"
                  "-Wmissing-format-attribute"
                  "-Wmissing-include-dirs"
                  "-Wno-c99-extensions"
                  ;; "-Wno-missing-field-initializers"
                  "-Wno-unused-parameter"
                  "-Wodr"
                  "-Wold-style-cast"
                  "-Wredundant-decls"
                  "-Wuninitialized"
                  "-Wunreachable-code"

                  "-Wmissing-field-initializers"
                  "-Wno-gnu-zero-variadic-macro-arguments"
                  "-Wunused-parameter"
                  "-fmodules-ts"
                  "-Wno-format-nonliteral"

                 ;;  ;; "-Wconversion"
                 ;;  ;; "-Wformat=2"
                 ;;  ;; "-Wmissing-format-attribute"
                 ;;  ;; "-fbuiltin-module-map"
                 ;;  ;; "-fcxx-modules"
                 ;;  ;; "-fimplicit-module-maps"
                 ;;  ;; "-fmodules"
                 ;;  ;; "-fmodules-decluse"
                 ;;  ;; "-fno-ident"
                  ;; debugging:
                  ;; "-g"
                  ;; "-Og"
                  ;; "-pg"
                  ]


;; [;; Good optimization options: -O3, -Os, or -Ofast.  See:

 :lflags-default ["-O1"
                  "-fPIC"
                  "-fdata-sections"
                  "-ffunction-sections"
                  "-flto"
                  "-fmodules-ts"
                  "-fno-exceptions"
                  "-fno-rtti"
                  "-fno-strict-aliasing"
                  "-fno-threadsafe-statics"
                  "-fstack-protector-all"
                  "-fvisibility-inlines-hidden"
                  "-fvisibility=hidden"
                  "-pedantic"
                  "-pipe"
                  "-std=c++2a"
                  "-stdlib=libc++"

                  ;; "-static" ; didn't work on macos
                  ;; "-fuse-ld=lld" ; didn't work on macos
                  ;; "-lc++abi"
                  ;; "-pthread"
                  ;; "-lm"
                  ;; "-lc"

                  ;; debugging
                  ;; -g -Og -pg

                  ;; This doesn't seem to work?
                  ;; -Wl,-dead_strip
                  ]

 ;; ["-O3"
                 ;;  "-fPIC"
                 ;;  "-fdata-sections"
                 ;;  "-ffunction-sections"
                 ;;  "-flto"
                 ;;  "-fmodules-ts"
                 ;;  "-fno-exceptions"
                 ;;  "-fno-rtti"
                 ;;  "-fno-strict-aliasing"
                 ;;  "-fno-threadsafe-statics"
                 ;;  "-fstack-protector-all"
                 ;;  "-fvisibility-inlines-hidden"
                 ;;  "-fvisibility=hidden"
                 ;;  "-pedantic"
                 ;;  "-pipe"
                 ;;  "-std=c++2a"
                 ;;  "-stdlib=libc++" ; broken on linux
                 ;;  ]
 }

basic (c++lib [] {:src ["basic.cc"]
                  :hdr ["basic.h"]})

basic_test (c++bin [basic] {:src ["basic_test.cc"]})

;; git clone https://github.com/google/googletest v/googletest
gtest-all (c++lib  []
                   {:hdr ["v/googletest/googletest/include/gtest/gtest.h"]
                    :src ["v/googletest/googletest/src/gtest-all.cc"]
                    :cflags ["-I" "v/googletest/googletest/include"
                             "-I" "v/googletest/googletest"
                             "-pthread"]})
gtest-main (c++lib  []
                    {:hdr ["v/googletest/googletest/include/gtest/gtest.h"]
                     :src ["v/googletest/googletest/src/gtest_main.cc"]
                     :cflags ["-I" "v/googletest/googletest/include"
                              "-I" "v/googletest/googletest"
                              "-pthread"]})
gmock-all (c++lib []
                  {:hdr ["v/googletest/googlemock/include/gmock/gmock.h"]
                   :src ["v/googletest/googlemock/src/gmock-all.cc"]
                   :cflags ["-I" "v/googletest/googletest/include"
                            "-I" "v/googletest/googletest"
                            "-I" "v/googletest/googlemock/include"
                            "-I" "v/googletest/googlemock"
                            "-pthread"]})

eden (c++lib [] {:hdr ["eden.h"] :src ["eden.cc"]})

eden_test (c++run [basic eden gtest-all gtest-main gmock-all]
                  {:src ["eden_test.cc"]
                   :cflags ["-I" "v/googletest/googletest/include"
                            "-I" "v/googletest/googletest"
                            "-I" "v/googletest/googlemock/include"
                            "-I" "v/googletest/googlemock"
                            "-pthread"]
                   :lib ["pthread"]})

bb_rule (c++lib [basic eden]
                {:src ["bb_rule.cc"] :hdr ["bb_rule.h"]})

bb_resolver (c++lib [basic eden bb_rule]
                    {:src ["bb_resolver.cc"] :hdr ["bb_resolver.h"]})

bb (c++bin [basic eden bb_rule bb_resolver]
           {:src ["bb_main.cc"]})

;; TODO: need a Rule for pre-compiled libraries that we just link to, so they
;; can be passed on as dependencies.
;; pthread (c++-pre-lib [] {:cflags #{["-pthread"]} :lib #{"pthread"}})
gtest (c++pile [gtest-all gtest-main gmock-all]
                 {:cflags [["-I" "v/googletest/googletest/include"]
                           ["-I" "v/googletest/googletest"]
                           ["-I" "v/googletest/googlemock/include"]
                           ["-I" "v/googletest/googlemock"]
                           ["-pthread"]]
                  :lib ["pthread"]})

topical (c++lib [basic eden]
                {:src ["topical.cc"] :hdr ["topical.h"]})

;; (shared-lib gmock [gtest-all gmock-all])
;; ar -rv libgmock.a gtest-all.o gmock-all.o

;; (c++lib gumbo []
;;         {:hdr ["v/gumbo/?"]})

prettyprint.gumbo (c++bin []
                          {:src ["v/gumbo/examples/prettyprint.cc"]
                           :cflags ["-I" ".usr/include"]
                           :lflags ["-L" ".usr/lib"]
                           :lib ["gumbo"]})
