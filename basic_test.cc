int main(int argc, char* argv[], char** envp) {
  basic::Runtime runtime(argc, argv, envp);
  const std::string home = runtime.home();
  for (const auto& kv : std::map<std::string, std::string>({
           {"foo", "foo"},
           {"", ""},
           {"~", home},
           {"~/foo", home + "/foo"},
           {"a~b", "a~b"},
           {"~a/", "~a/"},
       })) {
    const std::string& given = kv.first;
    const std::string& want = kv.second;
    const std::string& got = runtime.ExpandPath(given);
    if (want != got) {
      std::cout << "given: '" << given << "' -> got: '" << got << "', "
                << "want: '" << want << "'\n";
    }
  }
  return 0;
}
