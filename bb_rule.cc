#include <iostream>

namespace bb {

namespace {
eden::Err populateSources(const eden::Symbol name,
                          const std::map<std::string_view, eden::Node>& attrs,
                          std::vector<std::filesystem::path>* srcs) {
  srcs->clear();
  auto it = attrs.find(":src");
  if (it == attrs.end()) {
    return eden::Err(
        1, ":src key not found for target " + std::string(name.name()));
  }
  for (const eden::Node* node : it->second.Vector()) {
    if (!node->IsString()) {
      return eden::Err(
          1, "src has the wrong type " + std::string(node->Typename()));
    }
    srcs->push_back(node->AsString());
  }
  return eden::Err::OK;
}

std::vector<std::filesystem::path> computeIncludes(
    const std::vector<bb::Rule*>& deps,
    const std::map<std::string_view, eden::Node>& attrs) {
  std::vector<std::filesystem::path> includes;
  if (auto it = attrs.find(":inc"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      includes.emplace_back(x->AsString());
    }
  }
  for (const Rule* dep : deps) {
    if (dep->type() == Rule::Type::CPP_LIB) {
      const CppLib* upcasted_dep = static_cast<const CppLib*>(dep);
      for (const auto& include : upcasted_dep->header_files()) {
        includes.push_back(include);
      }
    }
  }
  if (auto it = attrs.find(":hdr"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      includes.emplace_back(x->AsString());
    }
  }
  return includes;
}

void addObjFiles(const std::vector<bb::Rule*>& deps,
                 const std::filesystem::path& out_dir,
                 std::vector<std::filesystem::path>* obj_files) {
  for (const Rule* dep : deps) {
    if (dep->type() == Rule::Type::CPP_LIB) {
      const CppLib* upcasted_dep = static_cast<const CppLib*>(dep);
      obj_files->push_back(upcasted_dep->ObjFile(out_dir));
    }
  }
}

}  // namespace

CppLib::CppLib(const eden::Symbol name,
               const std::map<std::string_view, eden::Node>& attrs)
    : name_(name), attrs_(attrs) {
  obj_file_ = std::string(name_.name()) + ".o";
  if (auto it = attrs_.find(":hdr"); it != attrs_.end()) {
    for (auto x : it->second.Vector()) {
      header_files_.emplace_back(x->AsString());
    }
  }
}

eden::Err CppLib::Init() {
  if (auto err = populateSources(name_, attrs_, &srcs_); !err.ok()) {
    return err;
  }
  return eden::Err::OK;
}

// TODO: Perhaps a "realization step" is needed to set up the temp directory and
// provide it to the calls that need a directory.
std::filesystem::path CppLib::ObjFile(
    const std::filesystem::path& obj_dir) const {
  return {obj_dir / obj_file_};
}

eden::Err CppLib::Derive(const std::vector<bb::Rule*>& deps
                         __attribute__((unused)),
                         basic::Runtime* runtime) {
  const std::filesystem::path out_dir =
      runtime->ExpandPath(attrs_.at(":out-dir").AsString());
  if (srcs_.empty()) {
    return eden::Err(1, ":src invalid for " + std::string(name_.name()));
  }
  const auto& includes = computeIncludes(deps, attrs_);
  const std::filesystem::path obj_file = out_dir / obj_file_;
  if (eden::Err err = compileCpp(srcs_, includes, obj_file, attrs_, runtime);
      !err.ok()) {
    return eden::Err(1, "[compiling] " + std::string(err.msg()));
  }
  return eden::Err::OK;
}

CppPile::CppPile(const eden::Symbol name,
                 const std::map<std::string_view, eden::Node>& attrs)
    : name_(name) {
  if (auto it = attrs.find(":lib"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      lib_set_.insert(x->AsString());
    }
  }
  if (auto it = attrs.find(":cflags"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      std::vector<std::string> v;
      for (auto y : x->Vector()) {
        v.push_back(y->AsString());
      }
      cflags_set_.insert(v);
    }
  }
}

eden::Err CppBin::Init() {
  if (auto err = populateSources(name_, attrs_, &srcs_); !err.ok()) {
    return err;
  }
  return eden::Err::OK;
}

eden::Err CppBin::Derive(const std::vector<bb::Rule*>& deps,
                         basic::Runtime* runtime) {
  const std::filesystem::path out_dir =
      runtime->ExpandPath(attrs_.at(":out-dir").AsString());
  const std::filesystem::path bin_dir =
      runtime->ExpandPath(attrs_.at(":bin-dir").AsString());
  const auto& includes = computeIncludes(deps, attrs_);
  const std::filesystem::path obj_file =
      out_dir / (std::string(name_.name()) + ".o");
  std::vector<std::filesystem::path> obj_files = {obj_file};
  addObjFiles(deps, out_dir, &obj_files);
  if (eden::Err err = compileCpp(srcs_, includes, obj_file, attrs_, runtime);
      !err.ok()) {
    return eden::Err(1, "[compiling] " + std::string(err.msg()));
  }
  const std::filesystem::path bin_file = bin_dir / name_.name();
  if (eden::Err err = linkCppBinary(obj_files, bin_file, attrs_, runtime);
      !err.ok()) {
    return eden::Err(1, "[linking] " + std::string(err.msg()));
  }
  return eden::Err::OK;
}

eden::Err CppRun::Init() {
  if (auto err = populateSources(name_, attrs_, &srcs_); !err.ok()) {
    return err;
  }
  return eden::Err::OK;
}

eden::Err CppRun::Derive(const std::vector<bb::Rule*>& deps,
                         basic::Runtime* runtime) {
  const std::filesystem::path out_dir =
      runtime->ExpandPath(attrs_.at(":out-dir").AsString());
  const std::filesystem::path bin_dir =
      runtime->ExpandPath(attrs_.at(":bin-dir").AsString());
  const auto& includes = computeIncludes(deps, attrs_);
  const std::filesystem::path obj_file =
      out_dir / (std::string(name_.name()) + ".o");
  std::vector<std::filesystem::path> obj_files = {obj_file};
  addObjFiles(deps, out_dir, &obj_files);

  if (eden::Err err = compileCpp(srcs_, includes, obj_file, attrs_, runtime);
      !err.ok()) {
    return eden::Err(1, "[compiling] " + std::string(err.msg()));
  }
  const std::filesystem::path bin_file = bin_dir / name_.name();
  if (eden::Err err = linkCppBinary(obj_files, bin_file, attrs_, runtime);
      !err.ok()) {
    return eden::Err(1, "[linking] " + std::string(err.msg()));
  }
  std::vector<std::string> args = {};
  if (std::string err_str = runtime->ForkExecWait(bin_file, {});
      !err_str.empty()) {
    return eden::Err(1, "[running] " + err_str);
  }
  return eden::Err::OK;
}

std::unique_ptr<Rule> CreateRule(
    const eden::Symbol rule_class, const eden::Symbol name,
    const std::map<std::string_view, eden::Node>& attrs) {
  static eden::Symbol kCppLibSym("c++lib");
  static eden::Symbol kCppBinSym("c++bin");
  static eden::Symbol kCppRunSym("c++run");
  static eden::Symbol kCppPileSym("c++pile");
  if (rule_class == kCppLibSym) {
    return std::make_unique<CppLib>(name, attrs);
  } else if (rule_class == kCppBinSym) {
    return std::make_unique<CppBin>(name, attrs);
  } else if (rule_class == kCppRunSym) {
    return std::make_unique<CppRun>(name, attrs);
  } else if (rule_class == kCppPileSym) {
    return std::make_unique<CppPile>(name, attrs);
  }
  return nullptr;
}

// Should copy to tmpdir, derive, store, record instance hash.
// Subsequent derivations get the transient files from the storage.

// TODO: Currently only the one src can be present.  Fix this.
eden::Err compileCpp(const std::vector<std::filesystem::path>& srcs,
                     const std::vector<std::filesystem::path>& includes,
                     const std::filesystem::path& obj_file,
                     const std::map<std::string_view, eden::Node>& attrs,
                     basic::Runtime* runtime) {
  // :compiler ""
  const std::string compiler_program = attrs.at(":compiler").AsString();
  std::vector<std::string> flags;
  for (const auto& include : includes) {
    flags.push_back("-include");
    flags.push_back(include);
  }

  // :cflags-default [""]
  if (auto it = attrs.find(":cflags-default"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      flags.push_back(x->AsString());
    }
  }
  // :cflags [""]
  if (auto it = attrs.find(":cflags"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      flags.push_back(x->AsString());
    }
  }
  std::string srcs_str;
  std::string sep = "";
  flags.push_back("-c");
  for (const std::filesystem::path& src : srcs) {
    flags.push_back(src);
    srcs_str += sep + std::string(src);
    sep = ", ";
  }
  flags.push_back("-o");
  flags.push_back(obj_file);

  if (auto it = attrs.find(":sim"); it != attrs.end() &&
                                    it->second.Holds<bool>() &&
                                    it->second.Get<bool>() == true) {
    std::cout << "  compiling (simulated) " + srcs_str << " => " << obj_file
              << "\n";
    flags.insert(flags.begin(), compiler_program);
    for (const auto& flag : flags) {
      std::cout << " " << flag;
    }
    std::cout << "\n";
    return eden::Err::OK;
  }
  std::cout << "  compiling " + srcs_str << " => " << obj_file << "\n";
  std::cerr << "    \033[38;5;238m# " << compiler_program;
  for (const auto& flag : flags) {
    std::cerr << " " << flag;
  }
  std::cerr << "\033[0m\n";
  std::string result = runtime->ForkExecWait(compiler_program, flags);
  if (!result.empty()) {
    return eden::Err(1, result);
  }
  return eden::Err::OK;
}

eden::Err linkCppBinary(const std::vector<std::filesystem::path>& oFiles,
                        const std::filesystem::path& binFile,
                        const std::map<std::string_view, eden::Node>& attrs,
                        basic::Runtime* runtime) {
  // Uses attrs: :linker, :lib, :lflags-default, :lflags.
  const std::string linker_program = attrs.at(":linker").AsString();
  std::vector<std::string> flags(oFiles.begin(), oFiles.end());
  flags.push_back("-o");
  flags.push_back(binFile);
  // :lib []
  if (auto it = attrs.find(":lib"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      flags.push_back("-l" + x->AsString());
    }
  }
  // :lflags-default []
  if (auto it = attrs.find(":lflags-default"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      flags.push_back(x->AsString());
    }
  }
  // :lflags []
  if (auto it = attrs.find(":lflags"); it != attrs.end()) {
    for (auto x : it->second.Vector()) {
      flags.push_back(x->AsString());
    }
  }
  std::cout << "  linking => " << binFile << "\n";
  std::cerr << "    \033[38;5;238m# " << linker_program;
  for (const auto& flag : flags) {
    std::cerr << " " << flag;
  }
  std::cerr << "\033[0m\n";
  runtime->ForkExecWait(linker_program, flags);
  return eden::Err::OK;
}

}  // namespace bb
